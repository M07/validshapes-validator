﻿/*  Copyright 2015 M07
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Caching;
using ValidShapes.Enum;
using ValidShapes.Models;
using ValidShapes.Models.Json.Send;

namespace ValidShapes.Helpers
{
    public static class ValidShapesHelper
    {
        private const string VALIDSHAPES_VALIDATE_URL = "http://{0}.validshapes.com/validate/{1}";
        private const string VALIDSHAPES_SERVERS_NAME_URL = "http://api.validshapes.com/servers/name";
        private const string SHAPES = "shapes";
        private const string AUDIO = "audio";

        public static String Captcha(ValidShapesModel model)
        {
            var content = "<div id='validshapes'></div>" +
                            "<script type='text/javascript'>" +
                                "var vsParams = { " + FromModelToJsParameters(model) + " };\n" +
                                "VS.generate(document.getElementById('validshapes'), vsParams);" +
                                "</script>  ";

            return content;
        }

        private static string FromModelToJsParameters(ValidShapesModel model)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("'publicKey':'{0}'", model.PublicKey);

            if (model.PositionAccuracy != ValidShapesModel.DefaultPositionAccuracy)
            {
                builder.AppendFormat(", 'positionAccuracy':{0}", model.PositionAccuracy);
            }

            if (model.AudioCharactersAccuracy != ValidShapesModel.DefaultAudioCharactersAccuracy)
            {
                builder.AppendFormat(", 'audioCharactersAccuracy':{0}", model.AudioCharactersAccuracy);
            }

            if (model.ShapesDensity != ValidShapesModel.DefaultShapesDensity)
            {
                builder.AppendFormat(", 'shapesDensity':{0}", model.ShapesDensity);
            }

            if (model.MaxBackgroundWidth != ValidShapesModel.DefaultMaxBackgroundWidth)
            {
                builder.AppendFormat(", 'maxBackgroundWidth':{0}", model.MaxBackgroundWidth);
            }

            if (model.MaxBackgroundHeight != ValidShapesModel.DefaultMaxBackgroundHeight)
            {
                builder.AppendFormat(", 'maxBackgroundHeight':{0}", model.MaxBackgroundHeight);
            }

            if (model.ColorTheme != ValidShapesModel.DefaultColorTheme)
            {
                builder.AppendFormat(", 'colorTheme':'{0}'", model.ColorTheme);
            }

            if (model.Lang != ValidShapesModel.DefaultLang)
            {
                builder.AppendFormat(", 'lang':'{0}'", model.Lang);
            }

            if (!string.IsNullOrEmpty(model.InstructionHelp))
            {
                builder.AppendFormat(", 'instructionHelp':'{0}'", model.InstructionHelp);
            }

            if (!string.IsNullOrEmpty(model.AudioHelp))
            {
                builder.AppendFormat(", 'audioHelp':'{0}'", model.AudioHelp);
            }

            return builder.ToString();
        }

        public static VsState CheckIfValid(string privateKey, NameValueCollection form)
        {
            var vs_audio = form["vs_audio"];
            var vs_xCoord = form["vs_xCoord"];
            var vs_yCoord = form["vs_yCoord"];
            var vs_token = form["vs_token"];
            var vs_captcha_type = form["vs_captcha_type"];
            var vs_server_name = form["vs_server_name"];

            var useCache = !string.IsNullOrEmpty(vs_server_name);

            if (string.IsNullOrEmpty(vs_token))
            {
                return VsState.MissingParameters;
            }

            var serversName = GetServersName(useCache);
            if (serversName == null)
            {
                return VsState.Success; // In case of our servers are down, we will allow the validation
            }

            if (!serversName.Any(x => x == vs_server_name))
            {
                return VsState.ServerIdError; // In the server name is unknown, we refuse the validation
            }

            try
            {
                using (var wb = new WebClient())
                {
                    ServicePointManager.Expect100Continue = false;
                    wb.Headers[HttpRequestHeader.ContentType] = "application/json";

                    string dataJson;
                    switch (vs_captcha_type)
                    {
                        case SHAPES:
                            if (string.IsNullOrEmpty(vs_xCoord) || string.IsNullOrEmpty(vs_yCoord))
                            {
                                return VsState.MissingParameters;
                            }
                            int xCoord, yCoord;
                            if (!int.TryParse(vs_xCoord, out xCoord) || !int.TryParse(vs_yCoord, out yCoord))
                            {
                                return VsState.MissingParameters;
                            }
                            dataJson = JsonHelper.To<JsonShapes>(new JsonShapes
                            {
                                privateKey = privateKey,
                                token = vs_token,
                                xCoord = xCoord,
                                yCoord = yCoord
                            });
                            break;
                        case AUDIO:
                            if (string.IsNullOrEmpty(vs_token) || string.IsNullOrEmpty(vs_audio))
                            {
                                return VsState.MissingParameters;
                            }
                            dataJson =  JsonHelper.To<JsonAudio>(new JsonAudio
                            {
                                privateKey = privateKey,
                                token = vs_token,
                                audioSolution = vs_audio
                            });
                            break;
                        default:
                            return VsState.MissingParameters;
                    }
                    var url = string.Format(VALIDSHAPES_VALIDATE_URL, vs_server_name, vs_captcha_type);
                    var response = wb.UploadString(url, "POST", dataJson);
                    var jsonResult = JsonHelper.From<Validate>(response);
                    if (!jsonResult.success)
                    {
                        var status = jsonResult.status;
                        switch (status)
                        {
                            case "WrongPosition":
                                return VsState.WrongPosition;
                            case "InvalidSolution":
                                return VsState.WrongAudioSolution;
                            case "InvalidToken":
                                return VsState.InvalidToken;
                            default:
                                return VsState.UnimplementedError; // Are you up to date?
                        }
                    }
                }
            }
            catch (Exception)
            {
                //If something goes wrong like ValidShapes servers crash, whe should not block users to validate the form.
                return VsState.Success;
            }

            return VsState.Success;
        }

        private static object ServersLock = new object();
        private const string CacheName = "ServersNameValidShapes";

        private static List<string> GetServersName(bool useCache)
        {
            try
            {
                object serverNames = HttpContext.Current.Cache[CacheName];
                if (serverNames == null || !useCache)
                {
                    HttpContext.Current.Cache.Remove(CacheName);
                    serverNames = RetrieveServersName();
                    HttpContext.Current.Cache.Insert(CacheName, serverNames, null, DateTime.Now.AddDays(1), Cache.NoSlidingExpiration);
                }
                return (List<string>)serverNames;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static List<string> RetrieveServersName()
        {
            try
            {
                using (var client = new WebClient())
                {
                    var response = client.DownloadString(VALIDSHAPES_SERVERS_NAME_URL);
                    var jsonResult = JsonHelper.From<Servers>(response);
                    var serversName = jsonResult.names;
                    return serversName;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
