﻿/*  Copyright 2015 M07
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ValidShapes.Enum;

namespace ValidShapes.Models
{
    public class ValidShapesModel
    {
        public string PublicKey { get; set; }
        public int PositionAccuracy { get; set; }
        public int AudioCharactersAccuracy { get; set; }
        public int ShapesDensity { get; set; }
        public int MaxBackgroundWidth { get; set; }
        public int MaxBackgroundHeight { get; set; }
        public ColorTheme ColorTheme { get; set; }
        public Language Lang { get; set; }
        public string InstructionHelp { get; set; }
        public string AudioHelp { get; set; }

        public const int DefaultPositionAccuracy = 2;
        public const int DefaultAudioCharactersAccuracy = 1;
        public const int DefaultShapesDensity = 2;
        public const int DefaultMaxBackgroundWidth = 320;
        public const int DefaultMaxBackgroundHeight = 110;
        public const ColorTheme DefaultColorTheme = ColorTheme.random;
        public static readonly Language DefaultLang = Language.AutoDetect;

        public ValidShapesModel(string publicKey)
        {
            PublicKey = publicKey;
            PositionAccuracy = DefaultPositionAccuracy;
            AudioCharactersAccuracy = DefaultAudioCharactersAccuracy;
            ShapesDensity = DefaultShapesDensity;
            MaxBackgroundWidth = DefaultMaxBackgroundWidth;
            MaxBackgroundHeight = DefaultMaxBackgroundHeight;
            ColorTheme = ColorTheme.random;
            Lang = Language.AutoDetect;
        }
    }
}