<?php

abstract class VsState
{
    const Success = 0;
    const WrongPosition = 1;
    const WrongAudioSolution = 2;
    const MissingParameters = 3;
    const ServerIdError = 4;
}

abstract class VsType {
 	const SHAPES = "shapes";
    const AUDIO = "audio";
}

interface ICacheProvider
{
	public function get($name);
	public function set($name, $value);
}


define( 'VALIDSHAPES_SERVERS_NAME', 'ValidShapes_ServersName' );
define( 'VALIDSHAPES_SERVERS_NAME_TIME_OUT', 'ValidShapes_serversNameTimeOut' );
define( 'VALIDSHAPES_VALIDATE_URL', 'http://%s.validshapes.com/validate/%s' );
define( 'VALIDSHAPES_SERVERS_NAME_URL', 'http://api.validshapes.com/servers/name' );
define( 'VALIDSHAPES_SERVERS_NAME_EXPIRED_TIME', 3600 * 24 ); // 1 day of expired time 

class ValidShapes_Validator {
	
	// function: 	CheckIfValid
	// definition:	Check if shapes are in good position
	// return:		VsState

	public static function CheckIfValid($privateKey, array $data, ICacheProvider $cacheProvider = null) {
		filter_var_array($data, FILTER_SANITIZE_STRING);

		$vs_audio = 		$data["vs_audio"];
		$vs_xCoord = 		$data["vs_xCoord"];
		$vs_yCoord = 		$data["vs_yCoord"];
		$vs_token = 		$data["vs_token"];
		$vs_captcha_type = 	$data["vs_captcha_type"];
		$vs_server_name = 	$data["vs_server_name"];
		
		$useCache = !empty($vs_server_name);
		$serversName = self::GetServersName($useCache, $cacheProvider);
		if ($serversName == null) {
			return VsState::Success; // In case of our servers are down, we will allow the validation
		}
		
		if (!in_array($vs_server_name, $serversName)) {
			return VsState::ServerIdError; // In the server name is unknown, we refuse the validation
		}
		
		switch ($vs_captcha_type)
		{
			case VsType::SHAPES:
				// add error checking here
				if (self::IsEmpty($vs_xCoord) || self::IsEmpty($vs_yCoord) || self::IsEmpty($vs_token)) {
					return VsState::MissingParameters;
				} 
				$data = array('xCoord' => intval($vs_xCoord), 'yCoord' => intval($vs_yCoord), 'token' => $vs_token, 'privateKey' => $privateKey);
				break;
			case VsType::AUDIO:
				if (self::IsEmpty($vs_token) || self::IsEmpty($vs_audio)) {
					return VsState::MissingParameters;
				} 
				$data = array('audioSolution' => $vs_audio, 'token' => $vs_token, 'privateKey' => $privateKey);
				break;
			default:
				return VsState::MissingParameters;
		}

		// use key 'http' even if we send the request to https://...
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/json; charset=utf-8\r\n",
				'method'  => 'POST',
				'content' => json_encode($data)
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents(sprintf(VALIDSHAPES_VALIDATE_URL, $vs_server_name, $vs_captcha_type), false, $context);

		// Check for errors
		if($result === FALSE) {
			//We already test that apiX.validshapes.com exists, we have any unexpected errors we allow the validation
			return VsState::Success;
		}
		
		// Decode the response
		$responseData = json_decode($result, TRUE);
		
		if(!$responseData['success']) {
			return $vs_captcha_type == "shapes" ? VsState::WrongPosition : VsState::WrongAudioSolution;
		}

		return VsState::Success;
	}

	// function: 	IsEmpty
	// definition:	Allow to know if a variable is empty or not (0 is not consider as empty)
	// return:		Bool - Is variable empty?
	
	private static function IsEmpty($variable) {
		return empty($variable) && strlen($variable) == 0;
	}

	// function: 	GetServersName
	// definition:	Get all servers name available in API
	// return:		List of servers name, can be null is server is unreachable
	
	private static function GetServersName($UseCache, ICacheProvider $cacheProvider) {
		if($UseCache && $cacheProvider) {
			$serversName = $cacheProvider->get(VALIDSHAPES_SERVERS_NAME);
			if (!self::IsEmpty($serversName)) {
				if($cacheProvider->get(VALIDSHAPES_SERVERS_NAME_TIME_OUT) > time()) {
					return $serversName;
				}				
			}
		}
		// use key 'http' even if we send the request to https://...
		$options = array(
			'http' => array(
				'method'  => 'GET'
			),
		);

		$context  = stream_context_create($options);
		$result = file_get_contents(VALIDSHAPES_SERVERS_NAME_URL, false, $context);

		// Check for errors
		if($result === FALSE) {
			return null;
		}
		$result = json_decode($result);
		$result = $result->names;

		if($cacheProvider) {
			$cacheProvider->set(VALIDSHAPES_SERVERS_NAME_TIME_OUT, time() + VALIDSHAPES_SERVERS_NAME_EXPIRED_TIME);
			$cacheProvider->set(VALIDSHAPES_SERVERS_NAME, $result);
		}

		return $result;
	}
}