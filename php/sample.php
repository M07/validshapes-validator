<?php
require_once( 'class.validshapes-validator.php' );

class CacheProvider implements ICacheProvider {
	public function get($name) {
		//Get value from cache
	}

	public function set($name, $value) {
		//Set value from cache
	}
}

$privateKey = "XXXXXXXXXXXXXXXXXXXXXX";

$result = ValidShapes_Validator::CheckIfValid($privateKey, $_POST, new CacheProvider());
